# Comunicação Arduíno com módulo acelerômetro MMA7361
***
Trabalho feito por **Emanuel Ozorio Dias** e **Daiara Paes da Rosa** para a disciplina de comunicação de dados.

## Objetivo
O objetivo é utilizar Arduíno, juntamente com um acelerômetro *MMA7361* para demonstrar graficamente em uma janela as posições de inclinação dos eixos X, Y e Z. Com esse projeto buscamos maior conhecimentos sobre Arduíno sua plataforma de comunicação em *C* e uma programação em *Python* junto as bibliotecas *Serial* e *PyGame* para comunicar com a Arduíno e demonstrar graficamente e simultaneamente na tela os valores dos eixos de inclinação obtidos com o Arduíno.

## Componentes
- 1 Arduino UNO;
- 1 Módulo de acelerômetro MMA7361;
- 1 Protoboard;
- Vários Jumpers;

## Projeto
A parte de desenvolvimento pode ser dividida em duas partes. Na primeira parte foi realizada a ligação do Arduíno com o acelerômetro *MMA7361* e feita a parte de recolhimento e tratamento dos dados. Na segunda, foi feito uma aplicação na linguagem *Python* utilizando a biblioteca *Serial* para pegar esses dados já tratados e, utilizando a biblioteca *Pygame*, demonstrar graficamente em forma de um retângulo na tela, as inclinações de cada eixo. Sendo o que os eixos X e Y foram usados para o movimento do retângulo e o eixo Z modifica o tamanho do mesmo.

### Ligação do módulo acelerômtro MMA7361 ao Arduíno
|Pino módulo| Função                            |Ligação ao Arduíno                         |
|:----------|:----------------------------------|:------------------------------------------|
|X          |voltagem de saída eixo X           |A0                                         |
|Y          |voltagem de saída eixo Y           |A1                                         |
|Z          |voltagem de saída eixo Z           |A2                                         |
|3V3        |Alimentação 3.3 Volts              |Ligar ao 3V3 do Arduíno (Melhor presisão)  |
|5V         |Alimentação 5 Volts                |Ligar ao 5V do Arduíno                     |
|ST         |Self Tests                         |Pino 12                                    |
|GS         |Seleção do modo de sensibilidade   |Pino 10                                    |
|0G         |Detecção de queda livre linear     |Pino 11                                    |
|SL         |Habilita o modo sleep              |Pino 13                                    |
|GND        |Ground                             |GND                                        |

## Exemplo de montagem
![Exemplo](https://gitlab.com/EmanuelOzorio/comunicacao-dados-2016/raw/master/img/exemplo.jpg)

## Placa montada
![Montagem1](https://gitlab.com/EmanuelOzorio/comunicacao-dados-2016/raw/master/img/montagem_1.jpg)
![Montagem2](https://gitlab.com/EmanuelOzorio/comunicacao-dados-2016/raw/master/img/montagem_2.jpg)

## Executando
![Executando](https://gitlab.com/EmanuelOzorio/comunicacao-dados-2016/raw/master/img/executando.png)

## Source
Os códigos fonte podem ser encontrados nos caminhos: 
[Código Arduino](https://gitlab.com/EmanuelOzorio/comunicacao-dados-2016/blob/master/src/arduino/codigo_arduino/codigo_arduino.ino) e 
[Código Python](https://gitlab.com/EmanuelOzorio/comunicacao-dados-2016/blob/master/src/python/codigo_visual_python.py).

## Resultado
Apesar de encontradas algumas dificuldades na implementação e principalmente no tratamento das informações obtidas do Arduíno para a transformação desses dados em um objeto visível e de fácil entendimento para o usuário, os resultados obtidos foram positivos. Também foi feita uma pesquisa pelos integrantes da equipe sobre o funcionamento dos eixos do acelerômetro. Referente a parte gráfica feita em *Phynton* foi mais facilmente implementado devido ao prévio conhecimento dessa linguagem de um dos integrantes da equipe. Importante destacar também a facilidade de integração da plataforma *Arduíno* com diferentes linguagens de programação, o que facilita o desenvolvimento de bons projetos como esse de forma rápida e sem muitos erros.

## Como rodar o código
Se você estiver utilizando UBUNTU:
- Na IDE do arduino carregar o código encontrado em *src\arduino\codigo_arduino\codigo_arduino.ino*;
- Com o comando ```sudo apt get install``` instalar as bibliotecas python3, python3-pygame, python3-re e python3-serial;
- Executar o código encontrado em *src\python\codigo_visual_python.py*.

## REFERENCIAS E LINKS UTÉIS
- Tutorial de como montar o arduino com o módulo mma7361: http://www.arduinoecia.com.br/2013/09/ligando-acelerometro-mma7361-no-arduino.html
- Documentação da Pygame: http://www.pygame.org/docs/
- Site util para testar e gerar o código de expressões regulares: https://regex101.com/