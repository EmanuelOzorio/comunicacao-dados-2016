#include <AcceleroMMA7361.h> //Carrega a biblioteca do MMA7361  

AcceleroMMA7361 accelero;
int x;
int y;
int z;

void setup()
{
  Serial.begin(9600);
  accelero.begin(13, 12, 11, 10, A0, A1, A2);
  //Seta a voltagem de referencia AREF como 3.3V
  accelero.setARefVoltage(3.3);
  //Serial.print("\tG*10^-2");
  //Seta a sensibilidade (Pino GS) para +/-6G    
  accelero.setSensitivity(LOW);
  accelero.calibrate();
}

void loop()
{
  x = accelero.getXAccel(); //Obtem o valor do eixo X
  y = accelero.getYAccel(); //Obtem o valor do eixo Y
  z = accelero.getZAccel(); //Obtem o valor do eixo Z
  Serial.print('x');
  Serial.print(x);
  Serial.print('y');
  Serial.print(y);
  Serial.print('z');
  Serial.print(z);
  Serial.print('\n');
  delay(100);
}
