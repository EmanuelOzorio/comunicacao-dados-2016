import serial
import re
import pygame

## Pygame
pygame.init() # Inicia a lib
screensize = [600, 480]
screen=pygame.display.set_mode(screensize) # Declara a tela

## Serial
## Inicia a lib na porta onde o arduino esta plugado
ser = serial.Serial('/dev/ttyACM0', 9600)
print('leu serial')
## Tabela com o regex para tratar o retorno do arduino
tabela = {'x': re.compile('x([-0-9]+)'),
      'y': re.compile('y([-0-9]+)'),
      'z': re.compile('z([-0-9]+)')}

x = 0
y = 0
z = 0

## Loop principal
while True:
  print('entrou no loop')
  # Evento de saida
  for event in pygame.event.get():
    if event.type==pygame.QUIT:
      sys.exit()
  print('sla')

  # Le a string retornada pelo arduino
  try:
    ser.inWaiting()

    s = ser.readline()
    s = str(s)

    print('pego string serial')

    # Trata a string separando o X, Y e Z
    if re.search(tabela['x'], s):
      x = re.search(tabela['x'], s).group(1)

    if re.search(tabela['y'], s):
      y = re.search(tabela['y'], s).group(1)

    if re.search(tabela['z'], s):
      z = re.search(tabela['z'], s).group(1)
  except:
    print('disconected')

  print('x: ', x, ' y: ', y, ' z: ', z)

  # Limpa a tela e desenha um retangulo com os 
  # valores X, Y e Z que o arduino retornou
  screen.fill([0,0,255])
  pygame.draw.rect(screen, (0, 255, 0), (screensize[1] / 2 + int(x), screensize[1] / 2 + int(y), int(z), int(z)), 0)
  pygame.display.flip()